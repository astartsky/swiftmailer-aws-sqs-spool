<?php
namespace Astartsky\Swiftmailer\AwsSqsSpool;

use Aws\Sqs\SqsClient;
use Guzzle\Service\Resource\Model;
use Psr\Log\LoggerInterface;
use Astartsky\MessagePacker\MessagePackerInterface;

class AwsSqsSpool extends \Swift_ConfigurableSpool
{
    const MIN_NUMBER_MESSAGES = 1;
    const MAX_NUMBER_MESSAGES = 5;
    const TIME_LIMIT = 5;

    protected $sqsClient;
    protected $queueUrl;
    protected $maxMessageSize;
    protected $logger;
    protected $packer;

    /**
     * @param SqsClient $sqsClient
     * @param MessagePackerInterface $packer
     * @param LoggerInterface $logger
     * @param array $queueOptions
     *
     * @throws \InvalidArgumentException When the queue options "url", "max_message_size" are not provided
     */
    public function __construct(SqsClient $sqsClient, MessagePackerInterface $packer, LoggerInterface $logger, $queueOptions)
    {
        $this->sqsClient = $sqsClient;
        $this->logger = $logger;
        $this->packer = $packer;

        foreach (array('url', 'max_message_size') as $key) {
            if (!isset($queueOptions[$key])) {
                throw new \InvalidArgumentException(sprintf('The queue option "%s" must be provided.', $key));
            }
        }

        $this->queueUrl = $queueOptions['url'];
        $this->maxMessageSize = $queueOptions['max_message_size'];
    }

    /**
     * {@inheritdoc}
     */
    public function isStarted()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function start()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function stop()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function queueMessage(\Swift_Mime_Message $message)
    {
        $this->logger->debug("Queuing message", array('message' => $message->getId()));

        try {
            $encodedMessage = $this->packer->pack($message);
        } catch (\Exception $e) {
            $this->logger->error("Queue message packing failed", array(
                'message_local_id' => $message->getId(),
                'error' => $e->getMessage()
            ));
            return false;
        }

        $size = strlen($encodedMessage);
        if ($size > $this->maxMessageSize * 1024) {
            $this->logger->error("Queue message size exceeded", array(
                'message_local_id' => $message->getId(),
                'size' => $size
            ));
            return false;
        }

        try {
            /** @var Model $response */
            $response = $this->sqsClient->sendMessage(array(
                'QueueUrl' => $this->queueUrl,
                'MessageBody' => $encodedMessage
            ));

            $this->logger->debug("Message queued", array(
                'message_local_id' => $message->getId(),
                'message_id' => $response->get('MessageId'),
                'request_id' => $response->getPath('ResponseMetadata/RequestId')
            ));

        } catch (\Exception $e) {
            $this->logger->error("Sending message to queue failed", array(
                'message_local_id' => $message->getId(),
                'error' => $e->getMessage()
            ));
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function flushQueue(\Swift_Transport $transport, &$failedRecipients = null)
    {
        $failedRecipients = (array) $failedRecipients;
        $count = 0;
        $time = time();

        $maxNumberOfMessages = static::MAX_NUMBER_MESSAGES;

        if ($this->getMessageLimit() > static::MIN_NUMBER_MESSAGES && $this->getMessageLimit() < static::MAX_NUMBER_MESSAGES) {
            $maxNumberOfMessages = $this->getMessageLimit();
        }

        $this->setTimeLimit(static::TIME_LIMIT);

        try {
            $this->logger->debug("Fetching queue");

            /** @var Model $result */
            $result = $this->sqsClient->receiveMessage(
                array(
                    'QueueUrl' => $this->queueUrl,
                    'MaxNumberOfMessages' => $maxNumberOfMessages
                )
            );

            $this->logger->debug(
                "Queue fetched",
                array(
                    'request_id' => $result->getPath('ResponseMetadata/RequestId')
                )
            );
        } catch (\Exception $e) {
            $this->logger->error(
                "Fetching queue failed",
                array(
                    'error' => $e->getMessage()
                )
            );
            return $count;
        }

        if (!$transport->isStarted()) {
            $transport->start();
        }

        if ($result->getPath('Messages')) {
            /** @var Model $message */
            foreach ($result->getPath('Messages') as $messageArray) {

                $this->logger->debug(
                    "Message received",
                    array(
                        'request_id' => $result->getPath('ResponseMetadata/RequestId'),
                        'message_id' => $messageArray['MessageId']
                    )
                );

                try {
                    /** @var \Swift_Message $message */
                    $message = $this->packer->unpack($messageArray['Body']);
                    $this->logger->debug(
                        "Message unpacked",
                        array(
                            'request_id' => $result->getPath('ResponseMetadata/RequestId'),
                            'message_id' => $messageArray['MessageId'],
                            'local_message_id' => $message->getId()
                        )
                    );
                } catch (\Exception $e) {
                    $this->logger->error(
                        "Message unpacking failed",
                        array(
                            'request_id' => $result->getPath('ResponseMetadata/RequestId'),
                            'error' => $e->getMessage()
                        )
                    );
                    continue;
                }

                $count += $transport->send($message, $failedRecipients);

                $this->logger->debug(
                    "Message sent",
                    array(
                        'request_id' => $result->getPath('ResponseMetadata/RequestId'),
                        'message_id' => $messageArray['MessageId'],
                        'local_message_id' => $message->getId()
                    )
                );

                try {
                    $this->sqsClient->deleteMessage(array(
                        'QueueUrl' => $this->queueUrl,
                        'ReceiptHandle' => (string) $messageArray['ReceiptHandle']
                    ));

                    $this->logger->debug(
                        "Message removed from queue",
                        array(
                            'request_id' => $result->getPath('ResponseMetadata/RequestId'),
                            'message_id' => $messageArray['MessageId'],
                            'local_message_id' => $message->getId()
                        )
                    );

                } catch (\Exception $e) {
                    $this->logger->error(
                        "Message removal from queue failed",
                        array(
                            'request_id' => $result->getPath('ResponseMetadata/RequestId'),
                            'message_id' => $messageArray['MessageId'],
                            'local_message_id' => $message->getId(),
                            'error' => $e->getMessage()
                        )
                    );
                    return false;
                }

                if ($this->getMessageLimit() && $count >= $this->getMessageLimit()) {

                    $this->logger->debug(
                        "Message limit reached",
                        array(
                            'request_id' => $result->getPath('ResponseMetadata/RequestId'),
                            'message_id' => $messageArray['MessageId'],
                            'limit' => $this->getMessageLimit()
                        )
                    );

                    return $count;
                }

                if ($this->getTimeLimit() && (time() - $time) >= $this->getTimeLimit()) {

                    $this->logger->debug(
                        "Time limit reached",
                        array(
                            'request_id' => $result->getPath('ResponseMetadata/RequestId'),
                            'message_id' => $messageArray['MessageId'],
                            'limit' => $this->getTimeLimit()
                        )
                    );

                    return $count;
                }
            }
        }

        return $count;
    }
}
